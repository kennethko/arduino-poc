

const int led = 13;
/* 
 * gnd
 * echo input
 * trig  output
 * vcc 5V
 */
const int echoPin = 8;
const int trigPin = 7;

void setup() {
  Serial.begin(9600);
  pinMode(led, OUTPUT);
}

void loop() {
  
  long duration, inches, cm;
  
  duration = distanceMs();  
  inches = usToInch(duration);
  cm = usToCm(duration);
 
  distanceLedSet(inches, cm);
  distancePrint(inches, cm);
 
}

void distanceLedSet(long in, long cm)
{
  int tOn = 25, tOff = 975;
  
  if (in == 0 && cm == 0) 
    return;
  
  if (in < 3) {
    tOn = 50;
    tOff = 950;
  } else if (in < 5) {
    tOn = 100;
    tOff = 900;
  } else if (in < 8) {
    tOn = 150;
    tOff = 850;
  } else if (in < 12) {
    tOn = 200;
    tOff = 800;
  } else if (in < 15) {
    tOn = 250;
    tOff = 750;
  } else if (in < 17) {
    tOn = 300;
    tOff = 700;
  } else if (in < 20) {
    tOn = 350;
    tOff = 650;
  } else if (in < 23) {
    tOn = 400;
    tOff = 600;
  } else {
    tOn = 800;
    tOff = 200;
  }
  
  digitalWrite(led, HIGH);
  delayMicroseconds(tOn);
  
  digitalWrite(led, LOW);
  delayMicroseconds(tOff);
  
  
}

long distanceMs()
{
  pinMode(trigPin, OUTPUT);
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(trigPin, LOW);
  
  pinMode(echoPin, INPUT);
  return pulseIn(echoPin, HIGH);
}

void distancePrint(long in, long cm) 
{   
  if (in != 0 || cm != 0) {
    Serial.print(in);
    Serial.print("in, ");
    Serial.print(cm);
    Serial.print("cm");
    Serial.println();
  } 
}

long usToInch(long us)
{
  long in;
  in = us / 74 / 2;
  if (in > 1000)
    in = 0;
  return in;
}
 
 
long usToCm(long us)
{
  long cm;
  cm = us / 29 / 2;
  if (cm > 100)
    cm = 0;
  return cm;
}
